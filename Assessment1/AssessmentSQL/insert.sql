-- insert into Client table
INSERT INTO Client
            (Client_id, Name, Email, Phone)
            VALUES
            (1, 'Gateway2000', 'HR@gateway.com', '01-625123132');

INSERT INTO Client
            (Client_id, Name, Email, Phone)
            VALUES
            (2, 'Avco Financial', 'Jan@avco.com', '01-782393282');
INSERT INTO Client
            (Client_id, Name, Email, Phone)
            VALUES
            (3, 'Xilinx', 'martin.s@xilinx.com', '01-6497439744');
INSERT INTO Client
            (Client_id, Name, Email, Phone)
            VALUES
            (4, 'JC Building service ltd', 'jc@hotmail.com', '058-3432823');
INSERT INTO Client
            (Client_id, Name, Email, Phone)
            VALUES                                                
            (5, 'Voxpro Communications', 'HR@voxpro.ie', '021-43788929');
INSERT INTO Client
            (Client_id, Name, Email, Phone)
            VALUES          
            (6, 'Mather Private', 'jim@mater.ie', '021-239843292');
INSERT INTO Client
            (Client_id, Name, Email, Phone)
            VALUES                       
            (7, 'Teva Pharmaceuticals', 'john@teva.ie', '058-4223242323');

-- insert into Agency table
INSERT INTO Agency
            (Agency_id, Name, Email, Phone, Web, Agency_type, Recruitment, Placement)
            VALUES 
            (1, 'Morgan McKinley', 'mail@email-morganmckinley.com', '01-543212344', 
            'http://www.morganmckinley.ie/','Rec/Pl', 'Recruitment','Placement');

INSERT INTO Agency
            (Agency_id, Name, Email, Phone, Web, Agency_type, Recruitment, Placement)
            VALUES 
            (2, 'ITContracting', 'Sean@ITContracting.ie', '058- 42923090', 
            'http://www.itcontracting.ie/', 'Pl', NULL, 'Placement');

INSERT INTO Agency
            (Agency_id, Name, Email, Phone, Web, Agency_type, Recruitment, Placement)
            VALUES 
            (3, 'Hibernan Evros', 'John@evros.ie', '01-234892392', 'http://www.evros.ie/',
            'Rec', 'Recruitment', NULL);

INSERT INTO Agency
            (Agency_id, Name, Email, Phone, Web, Agency_type, Recruitment, Placement)
            VALUES 
            (4, 'Ergo IT', 'martin@ergogroup.ie', '021-437892728', 'http://www.ergogroup.ie/',
            'Pl', NULL, 'Placement' );

INSERT INTO Agency
            (Agency_id, Name, Email, Phone, Web, Agency_type, Recruitment, Placement)
            VALUES
            (5, 'CPL Recruitment', 'martina@cpl.ie', '021-2347922929', 'https://www.cpl.ie/',
            'Rec', 'Recruitment', NULL);  
            
INSERT INTO Agency
            (Agency_id, Name, Email, Phone, Web, Agency_type, Recruitment, Placement)
            VALUES
            (6, 'Careerwise Recruitment', 'marie@careerwise.ie', '021-479842349',
            'http://careerwise.ie/', 'Rec', 'Recruitment', NULL);

-- insert into Rate
INSERT INTO Rate 
            (Rate_id, Monthly, Weekly, Daily, Hourly)
            VALUES
            (1,4000.00, 1000.00, 200.00, 25) ; 

INSERT INTO Rate 
            (Rate_id, Monthly, Weekly, Daily, Hourly)
            VALUES
            (2,5000, 1250.00, 250, 31.25) ;  
INSERT INTO Rate 
            (Rate_id, Monthly, Weekly, Daily, Hourly)
            VALUES
            (3,6000, 1500.00, 300.00, 37.50) ;
INSERT INTO Rate 
            (Rate_id, Monthly, Weekly, Daily, Hourly)
            VALUES
            (4, 7000, 1750, 350, 43.75) ;  
INSERT INTO Rate 
            (Rate_id,  Hourly)
            VALUES
            (5,50);
INSERT INTO Rate 
            (Rate_id, Hourly)
            VALUES
            (6,60);            

-- insert into Timesheet
INSERT INTO Timesheet
            (Start_Date, End_Date, Hour_Worked,Ts_Client_id,Ts_Agency_id)
            VALUES
			('6/Jan/2014', '30/May/2014', 600, 1,2);   

INSERT INTO Timesheet
            (Start_Date, End_Date, Hour_Worked,Ts_Client_id,Ts_Agency_id)
            VALUES
			('2/Jun/2014', '26/Dec/2014', 880,2, 2);                                                                                   
INSERT INTO Timesheet
            (Start_Date, End_Date, Hour_Worked,Ts_Client_id,Ts_Agency_id)
            VALUES
			('12/Jan/2015', '29/May/2015', 560, 3,1);                                                                                   						                                                                                
INSERT INTO Timesheet
            (Start_Date, End_Date, Hour_Worked,Ts_Client_id,Ts_Agency_id)
            VALUES
			('22/Jun/2015', '28/Aug/2015', 260, 4,3);                                                                                   
INSERT INTO Timesheet
            (Start_Date, End_Date, Hour_Worked,Ts_Client_id,Ts_Agency_id)
            VALUES
			('31/Aug/2015', '27/nov/2015', 220, 5,4);      
INSERT INTO Timesheet
            (Start_Date, End_Date, Hour_Worked,Ts_Client_id,Ts_Agency_id)
            VALUES
			('30/Nov/2015', '1/Apr/2016', 120, 6,5);    

-- insert into table Absence
INSERT INTO Absence
            (Absence_id, Start_Holiday_Date, End_Holiday_Date, Start_Sick_Date, 
            End_Sick_Date, Ts_start_date)
            VALUES
            (1, '14/Jun/2014', '28/Jun/2014', NULL, NULL,'2/Jun/2014');

INSERT INTO Absence
            (Absence_id, Start_Holiday_Date, End_Holiday_Date, Start_Sick_Date, 
            End_Sick_Date, Ts_start_date)
            VALUES
            (2, '14/May/2015', '28/May/2015', NULL, NULL,'12/Jan/2015'); 

INSERT INTO Absence
            (Absence_id, Start_Holiday_Date, End_Holiday_Date, Start_Sick_Date, 
            End_Sick_Date, Ts_start_date)
            VALUES
            (3, NULL, NULL, '3/Sep/2015', '6/Sep/2015','31/Aug/2015'); 

INSERT INTO Absence
            (Absence_id, Start_Holiday_Date, End_Holiday_Date, Start_Sick_Date, 
            End_Sick_Date, Ts_start_date)
            VALUES
            (4, NULL, NULL, '25/Dec/2015', '31/Dec/2015','30/Nov/2015');


-- insert into table Contract
INSERT INTO Contract
            (Contract_id, Con_client_id, Con_agency_id, Con_rate_id)
            VALUES
            (1,1,1,6);

INSERT INTO Contract
            (Contract_id, Con_client_id, Con_agency_id, Con_rate_id)
            VALUES
            (2,2,2,5);
INSERT INTO Contract
            (Contract_id, Con_client_id, Con_agency_id, Con_rate_id)
            VALUES
            (3,3,1,4);
INSERT INTO Contract
            (Contract_id, Con_client_id, Con_agency_id, Con_rate_id)
            VALUES
            (4,4,3,3); 
INSERT INTO Contract
            (Contract_id, Con_client_id, Con_agency_id, Con_rate_id)
            VALUES
            (5,5,4,2);
INSERT INTO Contract
            (Contract_id, Con_client_id, Con_agency_id, Con_rate_id)
            VALUES
            (6,6,5,1);                                              