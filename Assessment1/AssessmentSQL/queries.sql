
--Select Query to calculate gross earnings from hours worked and hourly rate for a par-ticular contract
---------------------------------------------------------------------------------------
SELECT Hour_worked * Hourly AS Result 
FROM Timesheet, Rate
WHERE Start_Date = '6/Jan/2014' and Rate_id = 6;
---------------------------------------------------------------------------------------



--Select Query to calculate days worked
--Number of days, not the worked days
---------------------------------------------------------------------------------------
SELECT End_Date - Start_Date AS NumberOfDays
       FROM Timesheet;
---------------------------------------------------------------------------------------



--Number of Working days roughly presuming we start on a Monday and full work weeks
--Difference minus two times the number of weeks nearest whole number(using ROUND)
--------------------------------------------------------------------------------------- 
SELECT Start_Date,End_Date,
       (End_Date-Start_Date)-2*ROUND((End_Date-Start_Date)/7)
       AS NumberWorkDays
       FROM Timesheet
       ORDER BY Start_Date,End_Date;
---------------------------------------------------------------------------------------



--Select Query to calculate earnings on a day rate
--applying rate to a single contract or apply a single rate to all contracts
--Single contract
---------------------------------------------------------------------------------------
SELECT Start_Date,End_Date,Daily,
       ((End_Date-Start_Date)-2*ROUND((End_Date-Start_Date)/7))*Daily
       AS Earnings_Daily_Rate
       FROM Timesheet, Rate
       WHERE Start_Date = '6/Jan/2014' and Rate_id = 4
       ORDER BY Start_Date,End_Date,Daily;
---------------------------------------------------------------------------------------



--All contracts with the same daily rate
---------------------------------------------------------------------------------------
SELECT Start_Date,End_Date,Daily,
       ((End_Date-Start_Date)-2*ROUND((End_Date-Start_Date)/7))*Daily
       AS Earnings_Daily_Rate
       FROM Timesheet, Rate
       WHERE Rate_id = 1
       ORDER BY Start_Date,End_Date,Daily;
---------------------------------------------------------------------------------------





--Select Query to calculate the number of weeks worked, again rough estimate
---------------------------------------------------------------------------------------
SELECT Start_Date, End_Date, 
       ROUND((End_Date - Start_Date)/7) AS Weeks_Worked
       FROM TimeSheet
       ORDER BY Start_Date, End_Date;
---------------------------------------------------------------------------------------



--Select Query to calculate the sum off pay for a weekly rate
---------------------------------------------------------------------------------------
SELECT Start_Date, End_Date, Weekly,
       (ROUND((End_Date - Start_Date)/7)) * Weekly 
       AS Earnings_Weekly_Rate
       FROM Timesheet, Rate
       WHERE Rate_id =3 and Start_Date ='22/Jun/2015'
       ORDER BY Start_Date, End_Date, Weekly;
---------------------------------------------------------------------------------------



--Select Query to apply a single weekly rate to all contracts
---------------------------------------------------------------------------------------
SELECT Start_Date, End_Date, Weekly,
       (ROUND((End_Date - Start_Date)/7)) * Weekly AS Earnings_Weekly_Rate
       FROM Timesheet, Rate
       WHERE Rate_id =3 
       ORDER BY Start_Date, End_Date, Weekly;
---------------------------------------------------------------------------------------



--Select Query to calculate number of months worked
---------------------------------------------------------------------------------------
SELECT Start_Date,End_Date,
       ROUND(MONTHS_BETWEEN(End_Date, Start_Date)) 
       AS Months_Worked
       FROM Timesheet
       ORDER BY Start_Date, End_Date;
---------------------------------------------------------------------------------------



--Select Query to work out the Earnings with month rate
---------------------------------------------------------------------------------------
SELECT Start_Date,End_Date,Monthly,
       (ROUND(MONTHS_BETWEEN(End_Date, Start_Date)))* Monthly
       AS Earnings_Monthly_Rate
       FROM Timesheet,Rate
       WHERE Rate_id = 4 AND Start_Date = '31/Aug/2015'
       ORDER BY Start_Date, End_Date, Monthly;
---------------------------------------------------------------------------------------



--Select to determine if there was an Absence in a Contract
---------------------------------------------------------------------------------------
SELECT Start_Holiday_Date, End_Holiday_Date, Start_Sick_Date, End_Sick_Date 
       FROM Absence
       WHERE Ts_start_date = '12/Jan/2015';
---------------------------------------------------------------------------------------


--Select to work out the holidays days in a contract
---------------------------------------------------------------------------------------
SELECT Start_Holiday_Date, End_Holiday_Date,
       (End_Holiday_Date - Start_Holiday_Date) AS Holidays_Days
       FROM Absence
       WHERE Ts_start_date = '12/Jan/2015' AND Start_Holiday_Date IS NOT NULL;

---------------------------------------------------------------------------------------



--Select to work out how many sick days in contract
---------------------------------------------------------------------------------------
SELECT Start_Sick_Date, End_Sick_Date,
       (End_Sick_Date - Start_Sick_Date) AS Sick_Leave_Days
       FROM Absence
       WHERE Ts_start_date = '31/Aug/2015' AND Start_Sick_Date IS NOT NULL;

---------------------------------------------------------------------------------------

 

--Select a list of Agencys contact information
---------------------------------------------------------------------------------------
SELECT Name, Email, Phone, Web FROM Agency
       ORDER BY Name;                                                                                                                  
---------------------------------------------------------------------------------------



--Select to work out the day of the week a contract starts on, 
---------------------------------------------------------------------------------------
Select Start_Date,
TO_CHAR(TO_DATE(Start_Date, 'DD/MM/YYYY'), 'Day') AS Start_Day
FROM Timesheet;
---------------------------------------------------------------------------------------



--Select to work our day of week contract finishes on
---------------------------------------------------------------------------------------
Select End_Date,
TO_CHAR(TO_DATE(End_Date, 'DD/MM/YYYY'), 'Day') AS End_Day
FROM Timesheet;
---------------------------------------------------------------------------------------



--Select a list of Client contacts
---------------------------------------------------------------------------------------
SELECT Name, Email, Phone
       FROM Client
       ORDER BY Name;
---------------------------------------------------------------------------------------
