-- CREATE TABLE STATMENTS
-- Client TABLE
CREATE TABLE Client
             (Client_id NUMBER(4) CONSTRAINT client_client_id_num_pk PRIMARY KEY,
             Name VARCHAR2(30) NOT NULL,
             Email VARCHAR2(30),
             Phone VARCHAR2(15));

-- Timesheet Table
CREATE TABLE Timesheet
             (Start_Date DATE CONSTRAINT ts_start_date_date_pk PRIMARY KEY,
              End_Date DATE CONSTRAINT ts_end_date_date_uk UNIQUE NOT NULL,
              Hour_Worked NUMBER(4) DEFAULT 0 NOT NULL,
              Ts_Client_id NUMBER(4) CONSTRAINT Ts_Client_id_num_fk 
                                     REFERENCES Client(Client_id),
              Ts_Agency_id NUMBER(4) CONSTRAINT Ts_Agency_id_num_fk 
                                     REFERENCES Agency(Agency_id));

-- Absence Table
CREATE TABLE Absence
             (Absence_id NUMBER(3) CONSTRAINT Absence_Absence_id_num_pk PRIMARY KEY,
              Start_Holiday_date DATE,
              End_Holiday_date DATE,
              Start_Sick_date DATE,
              End_Sick_date DATE,
              Ts_start_date DATE CONSTRAINT absence_ts_start_date_date_fk 
                                 REFERENCES Timesheet(Start_Date));

-- Agency Table
CREATE TABLE Agency
             (Agency_id NUMBER(4) CONSTRAINT Agency_agency_id_num_pk PRIMARY KEY,
              Name VARCHAR2(30) NOT NULL,
              Email VARCHAR2(30),
              Phone VARCHAR2(15),
              Web VARCHAR2(30),
              Agency_type VARCHAR2 (10),
              Recruitment VARCHAR2(12),
              Placement VARCHAR2(12));

-- Contract Table
CREATE TABLE Contract
             (Contract_id NUMBER(3) CONSTRAINT Contract_contract_id_num_pk PRIMARY KEY,
              Vat NUMBER(2) DEFAULT 23 NOT NULL,
              Con_client_id NUMBER(4) CONSTRAINT Contract_con_client_id_fk 
                                      REFERENCES Client(Client_id),
              Con_agency_id NUMBER(4) CONSTRAINT Contract_con_agency_id_fk 
                                      REFERENCES Agency(Agency_id),
              Con_rate_id NUMBER(4) CONSTRAINT Contract_con_rate_id_fk 
                                      REFERENCES Rate(Rate_id));

-- Rate Table
CREATE TABLE Rate
             (Rate_id NUMBER(4) CONSTRAINT Rate_rate_id_num_pk PRIMARY KEY,
              Monthly NUMBER(6,2) DEFAULT 0.0 NOT NULL,
              Weekly NUMBER(6,2) DEFAULT 0.0 NOT NULL,
              Daily NUMBER(5,2) DEFAULT 0.0 NOT NULL,
              Hourly NUMBER(5,2) DEFAULT 0.0 NOT NULL);             
